full:
	cargo build --release --features rtc

slim:
	cargo build --release 

dep:
	cargo update

run:
	cargo run --features rtc --release -- -s

test:
	cd ../ && bash scripts/run-api-tests.sh
	rm -f *.png

clean:
	cargo clean
