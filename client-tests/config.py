import subprocess
import os
import sys
import json

class Server:
    def __init__(self, meta: dict):
        self.url = meta.get('url')
        self.wsurl = meta.get('wsurl')
        self.serv_name = meta.get('name')

    def __str__(self) -> str:
        fields = {
            'url': self.url,
            'wsurl': self.wsurl,
            'name': self.serv_name
        }
        return str(fields)


class Admin:
    def __init__(self, user: dict, server: dict):
        self.id = user.get('id')
        self.name = user.get('name')
        self.permissions = user.get('permissions')
        self.secret = user.get('secret')
        self.status = user.get('status')
        self.jwt = None

        self.server = Server(server)

    def __str__(self) -> str:
        acc = {
            'id': self.id,
            'name': self.name,
            'permissions': self.permissions,
            'secret': self.secret,
            'jwt': self.jwt,
        }
        container = {'user': acc, 'server': str(self.server)}
        return str(container)

def create_admin() -> Admin :
    CARGO_BIN = os.getenv('CARGO_BIN')

    proc = subprocess.run(
        f'cargo run --release -- -c ../config.json'.split(),
        text=True, capture_output=True
    )
    try:
        raw = json.loads(proc.stdout)
        e = proc.stderr
        user = raw.get('user')
        server = raw.get('server')
        if user is None or server is None:
            print('stderr ', e)
            print('stdout ', proc.stdout)
            print(f'User/Server Data was not serializable => {raw}', file=sys.stderr)
            return None
        else:
            return Admin(user, server)
    except Exception as e:
        print(f'[create_admin] General exception caught in parsing => {e}', file=sys.stderr)
        print('stderr ', proc.stderr)
        print('stdout ', proc.stdout)
        return None

