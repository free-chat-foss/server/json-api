from requests import Session
import requests

NC = '\033[0m'
RED = '\033[1;31m'
GREEN = '\033[1;32m'
YELLOW = '\033[1;33m'

class Request:

    def __init__(self, url: str, method: str, qs: dict, headers: dict, hope: int, body=None, verbose=False):
        self.method = method
        self.url = url
        # query string parameters are appended to the url which is why  we do this
        self.qs = qs
        self.headers = headers
        self.body = body

        self.response = None
        self.hope = hope
        # This flag lets us control how much output we want to see
        self.verbose = verbose


    def __str__(self):
        return str(self.qs)

    @property
    def passing(self):
        if self.response is None:
            return False
        else:
            return self.response.status_code == self.hope

    def fire(self) -> requests.Response:
        try:
            if self.method == 'get':
                self.response = requests.get(self.url, headers=self.headers, params=self.qs)
            elif self.method == 'post':
                self.response = requests.post(self.url, headers=self.headers, params=self.qs, data=self.body)
            elif self.method == 'delete':
                self.response = requests.delete(self.url, headers=self.headers, params=self.qs)
            elif self.method == 'put':
                self.response = requests.put(self.url, headers=self.headers, params=self.qs, data=self.body)
            
            return self.response
        except Exception as e:
            print(e)
            return None

        

    def show_response(self):

        real_code = None
        if self.response is not None:
            real_code = self.response.status_code

        if self.hope != real_code:
            abstract = RED + 'Fail ' + NC + 'Expected ' + str(self.hope) + ' Got ' + str(real_code)
            if self.response is None:
                print('\tNo Response to show for')

            print(abstract)
            print('\t', self.method, ' ', self.url)
            if len(self.headers) != 0:
                print(YELLOW + '\tRequest-Headers ' + NC, self.headers)

            if len(self.qs) != 0:
                print(YELLOW + '\tQuery-Dictionary' + NC, self.qs)

            if self.body is not None:
                print(YELLOW + '\tRequest-Body' + NC, str(self.body))

            if self.verbose:
                print(f'\t{YELLOW}Response-Status{NC} {self.response.status_code}')
                print(f'\t{YELLOW}Response-Headers{NC} {self.response.headers}')
                print(f'\t{YELLOW}Response-Text{NC} {self.response.text}')

        else:
            print(f'{GREEN}Pass{NC} {self.method} {self.url}')
            if self.verbose:
                print(f'\t{YELLOW}Response-Status{NC} {self.response.status_code}')
                print(f'\t{YELLOW}Response-Headers{NC} {self.response.headers}')
                print(f'\t{YELLOW}Response-Text{NC} {self.response.text}')

