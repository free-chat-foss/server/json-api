use mysql_async::{Pool, params, Result, prelude::Queryable};

pub async fn listed(p: &Pool, id: u64, given_rng_value: &str) -> Result<bool> {
    let mut conn = p.get_conn().await?;
    let q = "SELECT rng FROM jwt WHERE id = :id";
    let row: Option<String> = conn.exec_first(q, params!{"id" => id}).await?;
    if let Some(value) = row { 
        Ok(value == given_rng_value)
    } else{
        Ok(false)
    }
}

pub async fn insert(p: &Pool, id: u64, given_rng_value: &str) -> Result<()> {
    let mut conn = p.get_conn().await?;
    let q = "INSERT INTO jwt (id, rng) VALUES(:id, :rng)
            ON DUPLICATE KEY UPDATE rng = :rng";
    let p = params!{
        "id" => id,
        "rng" => given_rng_value,
    };
    conn.exec_drop(q, p).await?;
    Ok(())
}
