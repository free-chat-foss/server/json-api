-- @id : id of the invite which is also its kill date

-- @uses : can be null which means it doesn't have a use limit

-- @expires: boolean that tells wether the key expires or not

CREATE TABLE IF NOT EXISTS `invites` (
	`id` BIGINT UNIQUE NOT NULL,
	`uses` BIGINT, 
	`expires` BOOLEAN NOT NULL,
	PRIMARY KEY( `id` )
);
