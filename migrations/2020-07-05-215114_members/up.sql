-- TODO: add rate limiter in some form
-- PERMISSIONS start at 0 and full perms => all F's
-- badges are stored as ad-hoc json for better operability in db-lib
-- mysql_async's json feature is basically 100% undocc'ed and i don't want to deal with it tbh
CREATE TABLE IF NOT EXISTS `members`(
	`id` BIGINT UNSIGNED NOT NULL,
	`secret` varchar(256) NOT NULL,
	`name` varchar(256) NOT NULL,
	`status` integer NOT NULL,
	`permissions` bigint UNSIGNED NOT NULL,
	`badge_ids` VARCHAR(2048) DEFAULT '[]',
	PRIMARY KEY( `id` , `secret` )
);
