-- everything else is nullable due to 
-- A) Flexibility
-- B) They're part of optional features(such as the wsurl which not all servers may have)
CREATE TABLE IF NOT EXISTS `neighbors`(
	`url` VARCHAR(512) UNIQUE NOT NULL,
	`wsurl` VARCHAR(512),
	`name` VARCHAR(512) NOT NULL,
	`description` VARCHAR(1024),
	`tags` VARCHAR(1024) DEFAULT '[]'
);
