CREATE TABLE IF NOT EXISTS `badges`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(256) NOT NULL,
    `color` INTEGER UNSIGNED NOT NULL DEFAULT 0xFFFFFFFF,
    `permissions` INTEGER UNSIGNED, -- nullable because this isn't logically applicable to every badge
    PRIMARY KEY(`id`)
);
