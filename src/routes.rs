type Rstr = &'static str;

pub const AUTH_LOGIN: Rstr      = "/login";             // requires @id @secret

pub const META: Rstr            = "/meta";              // @ perms none @ requires JWT however
pub const INVITE_CREATE: Rstr   = "/invite/create";     // @ perms::CREATE_INVITE
pub const INVITE_JOIN: Rstr     = "/join";              // @ none for new accounts

pub const CHANNELS_LIST: Rstr   = "/channels/list";     // requires none
pub const CHANNELS_CREATE: Rstr = "/channels/create";   // requires @name @kind perms::CREATE_CHANNEl
pub const CHANNELS_DELETE: Rstr = "/channels/delete";   // requires @name perms::DELETE_CHANNEL

pub const MESSAGE_SEND: Rstr        = "/message/send";      // requires @content perms::MESSAGE_SEND
pub const MESSAGE_TIME_RANGE: Rstr  = "/message/get_range";      // requires @channel(id) @start-time @end-time
pub const MESSAGE_LAST_N: Rstr      = "/message/recent";    // requires @channel_id requires @limit(1..100)

pub const GET_ONLINE_MEMBERS: Rstr  = "/members/get_online"; // requires none
pub const GET_MEMBER: Rstr          = "/members/single";    // requires @member_id
pub const GET_MYSELF: Rstr          = "/members/me";         // @requires none
pub const SELF_UPDATE_NICKNAME: Rstr= "/members/me/nickname";


// BADGE ROUTES
pub const NEW_BADGE: Rstr          = "/badge/new";
pub const DELETE_BADGE: Rstr       = "/badge/delete";
pub const LIST_BADGE: Rstr         = "/badge/list";
pub const UPDATE_COLOR_BADGE: Rstr       = "/badge/update/color";
pub const UPDATE_NAME_BADGE: Rstr        = "/badge/update/name";
pub const UPDATE_PERMS_BADGE: Rstr       = "/badge/update/perms";

// ADMIN ROUTES
pub const SET_PERMS_BY_ADMIN: Rstr = "/admin/setpermisions";    // @requires perms::ADMIN
pub const SET_NEW_ADMIN: Rstr      = "/owner/newadmin";         // @requiers: owner perms

// Server -> Server Routes
pub const GET_NEIGHBORS: Rstr       = "/neighbor/list"; // @requires: none @note must be a member for this list
pub const ADD_NEIGHBOR: Rstr        = "/neighbor/add"; // @requires: perm::add_neighbor
pub const UPDATE_NEIGHBOR: Rstr     = "/neighbor/update"; // @requires perms::add_neighbor @url(unique)
pub const REMOVE_NEIGHBOR: Rstr     = "/neighbor/delete"; // @requires perms::add_neighbor @url(unique)

pub fn is_open(path: &str) -> bool {
    return path.starts_with(INVITE_JOIN) || path.starts_with("/meta");
}

pub fn requires_perms(path: &str) -> bool {
    return match path {
        /* These routes _don't_ require any permissions */
        AUTH_LOGIN | META | CHANNELS_LIST | GET_MYSELF | GET_NEIGHBORS => false,
        _ => true
    }
}
