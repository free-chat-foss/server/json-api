table! {
    channels (id) {
        id -> Unsigned<Bigint>,
        name -> Varchar,
        description -> Nullable<Varchar>,
        kind -> Integer,
    }
}

table! {
    invites (id) {
        id -> Bigint,
        uses -> Nullable<Bigint>,
        expires -> Bool,
    }
}

table! {
    jwt (id) {
        id -> Unsigned<Bigint>,
        token -> Varchar,
    }
}

table! {
    members (id, secret) {
        id -> Unsigned<Bigint>,
        secret -> Varchar,
        name -> Varchar,
        joindate -> Bigint,
        status -> Integer,
        permissions -> Unsigned<Bigint>,
    }
}

table! {
    messages (id) {
        id -> Unsigned<Bigint>,
        time -> Bigint,
        content -> Varchar,
        author_id -> Unsigned<Bigint>,
        channel_id -> Unsigned<Bigint>,
    }
}

joinable!(messages -> channels (channel_id));

allow_tables_to_appear_in_same_query!(
    channels,
    invites,
    jwt,
    members,
    messages,
);
